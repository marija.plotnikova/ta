package intro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class HW1 {

    public static void main(String[] args) throws IOException {

        int[] firstTaskResult = generateRandomizedArray();
        Set<Integer> secondTaskResult = sortArray(firstTaskResult);
        threeArrays(secondTaskResult);
    }

    public static int[] generateRandomizedArray() throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int arrayLength; //integer which will store user entered value

        do {
            System.out.println("Enter number: ");
            String lengthStr = reader.readLine(); // get user input string
            arrayLength = Integer.parseInt(lengthStr); // parse to int and save in variable

            if (arrayLength > 100 || arrayLength < 1) {
                System.out.println("Please enter number from 1 to 100");
            }

        } while (arrayLength > 100 || arrayLength < 1); // cycle will end if entered number will be valid

        int[] arr = new int[arrayLength]; // array length = entered value

        System.out.println("\n" + "1. Random numbers:");

        // print random numbers from 0 to 99
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) Math.round(Math.random() * 100);
            System.out.println(arr[i]);
        }

        return arr;
    }

    public static Set<Integer> sortArray(int[] firstTaskResult) {

        // int to Integer to work with Sets
        Integer[] boxedArray = Arrays.stream(firstTaskResult)
                .boxed()
                .toArray(Integer[]::new);

        // TreeSet
        List<Integer> list = Arrays.asList(boxedArray);
        Set<Integer> set = new TreeSet<Integer>(list);

        System.out.println("\n" + "2. Unique numbers in ascending order:");

        for(Integer sortedNum : set) {
            System.out.println(sortedNum);
        }

        return set;
    }

    public static void threeArrays(Set<Integer> secondTaskResult) {

        // convert secondTaskResult to int array
        int[] arr1 = secondTaskResult.stream().mapToInt(Integer::intValue).toArray();
        int[] arr2 = new int[arr1.length];

        // arr2 stores ints from arr1 in descending order
        int j = 0;
        for (int i = arr1.length-1; i >= 0; i--) {
                arr2[j] = arr1[i];
                j++;
        }

        ArrayList<Integer> compositeNumbers = new ArrayList<>();

        // add composite/neutral numbers to new array
        for (int v : arr2) {
            if (v <= 1) { // 0 and 1 are neutral numbers
                compositeNumbers.add(v);
            }
            for (int n = 2; n <= v / 2; n++) {
                if (v % n == 0) {
                    compositeNumbers.add(v);
                    break;
                }
            }
        }

        List<Integer> initialNumbers = new ArrayList<>(arr2.length);
        for (int i : arr2) {
            initialNumbers.add(i);
        }

        // remove composite numbers from initial list
        initialNumbers.removeAll(compositeNumbers);

        System.out.println("\n" + "3.1 Prime numbers in descending order:");

        for(Integer prime : initialNumbers) {
            System.out.println(prime);
        }

        // only 1d/2d numbers from secondTaskResult

        List<Integer> singleDigits = new ArrayList<>();
        List<Integer> doubleDigits = new ArrayList<>();


        for(Integer z : secondTaskResult) {
            if (z.toString().length() == 1) {
                singleDigits.add(z);
            }
            else if (z.toString().length() == 2) {
                doubleDigits.add(z);
            }
        }

        System.out.println("\n" + "3.2 One digit numbers:");

        for(Integer h : singleDigits) {
            System.out.println(h);
        }

        System.out.println("\n" + "3.3 Two digit numbers:");

        for(Integer u : doubleDigits) {
            System.out.println(u);
        }


        //TODO: error message for empty results
        }
    }

