package intro;

public class DefaultVar {

    static byte byteVar;
    static int intVar = 123;

    public static void main (String[] args) {

        System.out.println (byteVar);
        System.out.println (intVar);
    }
}
