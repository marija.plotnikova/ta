package intro;

public class StringTest {

    public static void main (String[] args) {

        String str1 = "one";
        String str2 = new String ("two");

        String sobaka = "dlinnoe, veseloe i kodit";
        String kot = "";
        String homjak = " dlinnoe, veseloe i kodit  ";

        System.out.println(sobaka.length());
        System.out.println(sobaka.charAt(23));
        System.out.println(sobaka.concat(" :)"));
        System.out.println(sobaka.contains(" i "));
        System.out.println(sobaka.startsWith("dlin"));
        System.out.println(sobaka.endsWith("eeff"));
        System.out.println(sobaka.equals("dlinnoe, veseloe i kodit"));
        System.out.println(sobaka.equals(homjak));

        System.out.println(kot.indexOf("h"));
        System.out.println(kot.isEmpty());

        System.out.println(homjak.trim());

        String i = "choco.woko";
        System.out.println(i.replace(".", "_"));

        String x = "abcdefghijkl";
        System.out.println(x.substring(5));
        System.out.println(x.substring(2,8));

        char[] chociWoki = x.toCharArray();

        for (int j = 0; j < chociWoki.length; j++) {
            System.out.println("Index " + j + " is " + chociWoki[j]);
        }

        String ab = "aBcdEf";
        System.out.println(ab.toUpperCase());
        System.out.println(ab.toLowerCase());

        String a = "0123456789abc";
        //char c = a[10];
        //System.out.println(c);
        char c = a.charAt(10);
        System.out.println(c);
    }
}
