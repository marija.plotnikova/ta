package intro;

public class Random {

    public static void main (String[] args) {

        double x = 2 - 1.2;
        double y = x;
        System.out.println(x);

        for (int i = 0, j = 10; i <= 10; i ++, j --) {
            System.out.println (i); // печатать i, идущую от 0 к 10
            System.out.println (j); // печатать j, идущую от 10 к 0
        }

    }
}
