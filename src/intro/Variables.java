package intro;

public class Variables {

    public static void main (String[] args) {

        byte byteValue = 127;
        System.out.println(byteValue);

        short shortValue = 30;
        System.out.println(shortValue);

        int intValue = 1000303;
        System.out.println("Int value is" + intValue);

        long longValue = 949439483L;
        System.out.println(longValue);

        float floatValue = 1.1f;
        System.out.println(floatValue);

        double doubleValue = 1.1;
        System.out.println(doubleValue);

        boolean booleanValue = true;
        System.out.println(booleanValue);

        String charValue = "trr";
        System.out.println(charValue);

        int i1 = 1;

    }
}
