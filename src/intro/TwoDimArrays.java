package intro;
import java.util.Arrays;

public class TwoDimArrays {
    public static void main(String[] args) {
        String[] nums = {"sf", "1"};

        for (String n: nums) {
            System.out.println (n);
        }

        for (int i = 0; i < nums.length; i++) {
            System.out.println (nums[i]);
        }

        int[] grades = {8, 9, 7, 10};
        System.out.println(Arrays.toString(grades));


        String[][] obed = {{"sup", "salat"}, {"desert", "sok"}, {"tort", "buterbrod"}};


        int numRows = obed.length;
        int numCols = obed[0].length;

        System.out.println("Number of rows: " + numRows);
        System.out.println("Number of cols: " + numCols);

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                System.out.println(obed[i][j]);
            }

            System.out.println();
        }


    }

}
